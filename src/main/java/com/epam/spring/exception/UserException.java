package com.epam.spring.exception;

import org.apache.log4j.Logger;

public class UserException extends Exception
{
    private static final Logger logger = Logger.getLogger("BusinessLayerLogger");
    private int status = 200;
    public UserException(String message) {
        super(message);
    }

    public UserException(String message, int status) {
        super(message);
        this.status = status;
        logger.error(message + " Status:"+status);
    }

    public int getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "UserException{" +
                " status= " + status +
                ", message= " + this.getMessage()+
                '}';
    }
}
