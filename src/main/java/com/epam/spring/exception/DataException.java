package com.epam.spring.exception;

import org.apache.log4j.Logger;

public class DataException extends Exception {

    private static final Logger logger = Logger.getLogger("BusinessLayerLogger");
    private int status = 200;

    public DataException(String message) {
        super(message);
    }

    public DataException(String message, int status) {
        super(message);
        this.status = status;
        logger.error(message + " Status:"+status);
    }

    public int getStatus() {
        return status;
    }
}
