package com.epam.spring.model;

import javax.persistence.*;

@javax.persistence.Entity
@Table(name="logs")
public class Log extends Entity
{
    @Id
    @GeneratedValue
    @Column(name = "logid")
    private long logId;
    @ManyToOne
    @JoinColumn(name = "userid")
    private User user;
    @Column(name = "text")
    private String text;


    public Log(long logId, User user, String text) {
        this.logId = logId;
        this.user = user;
        this.text = text;
    }

    public Log() {
    }

    public Log(User user, String text) {
        this.user = user;
        this.text = text;
    }
    public long getLogId() {
        return logId;
    }

    public User getUser() {
        return user;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "Log{" +
                "logId=" + logId +
                ", userId=" + user +
                ", text=" + text +
                " }";
    }
}
