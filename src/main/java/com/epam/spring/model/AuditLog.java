package com.epam.spring.model;

import javax.persistence.*;

@javax.persistence.Entity
@Table(name="AuditLogs")
public class AuditLog extends Entity
{
    @Id
    @GeneratedValue
    @Column(name = "auditid")
    private long auditId;
    @Column(name = "audit")
    private String audit;

    public AuditLog(String audit) {
        this.audit = audit;
    }

    public AuditLog() {
    }

    public long getAuditId() {
        return auditId;
    }

    public String getAudit() {
        return audit;
    }
}
