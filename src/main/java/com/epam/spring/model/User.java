package com.epam.spring.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@javax.persistence.Entity
@Table(name = "users")
public class User extends Entity
{
    @Id
    @GeneratedValue
    @Column(name = "userid")
    private long userId;
    @Column(name = "firstname")
    private String firstName;
    @Column(name = "lastname")
    private String lastName;
    @Column(name = "nickname")
    private String nickName;
    @Column(name = "password")
    private String password;
    @ManyToOne(fetch = FetchType.EAGER,cascade = {CascadeType.MERGE,CascadeType.DETACH,CascadeType.REFRESH,CascadeType.REMOVE})
    @JoinColumn(name="roleid")
    private Role role;
    @Column(name = "isblocked")
    private boolean isBlocked;

    @OneToMany(mappedBy = "user",fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private Collection<Bookmark> bookmarks;

    @OneToMany(mappedBy = "user",fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private Collection<Log> logs;


    public User(String firstName, String lastName, String nickName, String password, Role role) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickName = nickName;
        this.password = password;
        this.role = role;
    }

    public User(String firstName, String lastName, String nickName,
                String password, Role role, boolean isBlocked) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickName = nickName;
        this.password = password;
        this.role = role;
        this.isBlocked = isBlocked;
    }

    public User() {
    }

    public long getUserId() { return userId; }

    public String getFirstName() { return firstName; }

    public String getLastName() { return lastName; }

    public String getNickName() { return nickName; }

    public String getPassword() { return password; }

    public Role getRole() { return role; }

    public boolean isBlocked() { return isBlocked; }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", nickName='" + nickName + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", isBlocked=" + isBlocked +
                '}';
    }

}
