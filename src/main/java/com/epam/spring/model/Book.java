package com.epam.spring.model;


import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Collection;

@javax.persistence.Entity
@Table(name = "books")
@NamedQuery(name = "Book.findAllBookByUserBookMark" ,
        query = "SELECT b FROM Book b WHERE b.bookId IN (SELECT bm.book.bookId FROM Bookmark bm WHERE bm.user.userId = ?1)")
public class Book extends Entity
{
    @Id
    @GeneratedValue
    @Column(name = "bookid")
    private long bookId; //TODO ASK ANDREW
    @Column(name = "bookname")
    private String bookName;
    @Column(name = "releaseyear")
    private int releaseYear;
    @Column(name = "pagecount")
    private int pageCount;
    @Column(name = "ISBN")
    private String ISBN;
    @Column(name = "publisher")
    private String publisher;
    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.DETACH,CascadeType.REFRESH})
    @JoinColumn(name = "author")
    private Author author;

    @OneToMany(mappedBy = "book",fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private Collection<Bookmark> bookmarks;


    public Book(String bookName, int releaseYear, int pageCount, String ISBN, String publisher, Author author) {
        this.bookName = bookName;
        this.releaseYear = releaseYear;
        this.pageCount = pageCount;
        this.ISBN = ISBN;
        this.publisher = publisher;
        this.author = author;
    }

    public Book() {
    }

    public long getBookId() {
        return bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public int getPageCount() {
        return pageCount;
    }

    public String getISBN() {
        return ISBN;
    }

    public String getPublisher() {
        return publisher;
    }

    public Author getAuthor() {
        return author;
    }


    @Override
    public String toString() {
        return "Book{" +
                "bookId=" + bookId +
                ", bookName='" + bookName +
                ", releaseYear=" + releaseYear +
                ", pageCount=" + pageCount +
                ", ISBN='" + ISBN +
                ", publisher='" + publisher +
                ", authorId=" + author.getAuthorId() +
                '}';
    }
}
