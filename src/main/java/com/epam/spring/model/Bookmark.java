package com.epam.spring.model;

import javax.persistence.*;

@javax.persistence.Entity
@Table(name = "bookmarks")
public class Bookmark extends Entity
{
    @Id
    @GeneratedValue
    @Column(name="bookmarkid")
    private long bookmarkId;
    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.DETACH,CascadeType.REFRESH,CascadeType.REMOVE})
    @JoinColumn(name = "userid")
    private User user;
    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.DETACH,CascadeType.REFRESH,CascadeType.REMOVE})
    @JoinColumn(name = "bookid")
    private Book book;
    @Column(name = "pagenumber")
    private int pageNumber;

    public Bookmark(long bookmarkId, User user, Book book, int pageNumber) {
        this.bookmarkId = bookmarkId;
        this.user = user;
        this.book = book;
        this.pageNumber = pageNumber;
    }

    public Bookmark() {
    }

    public Bookmark(User user, Book book, int pageNumber) {
        this.user = user;
        this.book = book;
        this.pageNumber = pageNumber;
    }

    public long getBookmarkId() {
        return bookmarkId;
    }

    public User getUser() {
        return user;
    }

    public Book getBook() {
        return book;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    @Override
    public String toString() {
        return "Bookmark{" +
                "bookmarkId=" + bookmarkId +
                ", userId=" + user +
                ", bookId=" + book +
                ", pageNumber=" + pageNumber +
                '}';
    }
}
