package com.epam.spring.model;


import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@javax.persistence.Entity
@Table(name = "authors")
public class Author extends Entity
{
    @Id
    @GeneratedValue
    @Column(name = "authorid")
    private long authorId;
    @Column(name = "name")
    private String name;
    @Column(name = "secondname")
    private String secondName;
    @Column(name = "lastname")
    private String lastName;
    @Column(name = "dob")
    private Date dob;

    @OneToMany(mappedBy = "author",fetch = FetchType.EAGER,cascade = {CascadeType.MERGE,CascadeType.DETACH,CascadeType.REFRESH,CascadeType.REMOVE})
    @Fetch(value = FetchMode.SUBSELECT)
    private Collection<Book> books;

    public Author() {
    }


    public Author(String name, String secondName, String lastName, Date dob) {
        this.name = name;
        this.secondName = secondName;
        this.lastName = lastName;
        this.dob = dob;
    }

    public long getAuthorId() {
        return authorId;
    }

    public String getName() {
        return name;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getDob() {
        return dob;
    }

    @Override
    public String toString() {
        return "Author{" +
                "authorId=" + authorId +
                ", name='" + name + '\'' +
                ", secondName='" + secondName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dob=" + dob +
                '}';
    }
}
