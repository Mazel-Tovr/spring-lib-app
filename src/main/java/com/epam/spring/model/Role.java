package com.epam.spring.model;


import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Collection;

@javax.persistence.Entity
@Table(name = "roles")
public class Role extends Entity
{
    @Id
    @GeneratedValue
    @Column(name = "roleid")
    private int roleId;
    @Column(name = "type")
    @Enumerated(EnumType.STRING )
    private EnumRole type;

    @OneToMany(mappedBy = "role",fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private Collection<User> users;

    public Role(int roleId, EnumRole type) {
        this.roleId = roleId;
        this.type = type;
    }

    public Role() {
    }

    public Role(EnumRole type) {
        this.type = type;
    }

    public int getRoleId() {
        return roleId;
    }

    public EnumRole getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Role{" +
                "roleId=" + roleId +
                ", type=" + type +
                '}';
    }
}
