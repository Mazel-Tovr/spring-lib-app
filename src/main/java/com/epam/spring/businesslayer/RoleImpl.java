package com.epam.spring.businesslayer;

import com.epam.spring.businesslayer.interfaces.IRoleService;
import com.epam.spring.dao.interfaces.IRoleOperations;
import com.epam.spring.exception.DataException;
import com.epam.spring.model.EnumRole;
import com.epam.spring.model.Role;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.epam.spring.exception.ExceptionsCods.*;
@Service
public class RoleImpl implements IRoleService
{
    @Autowired
    private IRoleOperations roleOperations;

    private static final Logger logger = Logger.getLogger("BusinessLayerLogger");

    @Override
    public Role getRole(int roleId) throws DataException {
        Optional<Role> role =  roleOperations.findById(roleId);
        if(!role.isPresent()) throw new DataException("Role with such id doesn't exist",ROLE_DOES_NOT_EXIST);
        logger.info("Role gotten");
        return role.get();
    }

    @Override
    public Role addRole(EnumRole type) {

        logger.info("Role added");
        return roleOperations.save(new Role(type));

    }

    @Override
    public void dropRole(int roleId) throws DataException {
       roleOperations.deleteById(roleId);
        logger.info("Role deleted");
    }
}
