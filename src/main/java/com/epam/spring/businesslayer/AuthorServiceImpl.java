package com.epam.spring.businesslayer;


import com.epam.spring.businesslayer.interfaces.IAuthorService;
import com.epam.spring.dao.interfaces.IAuthorOperations;
import com.epam.spring.exception.DataException;
import com.epam.spring.model.Author;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import static com.epam.spring.exception.ExceptionsCods.*;

@Service
public class AuthorServiceImpl implements IAuthorService {

    @Autowired
    private IAuthorOperations authorOperations;

    private static final Logger logger = Logger.getLogger("BusinessLayerLogger");
    private static final Pattern DATE_PATTERN = Pattern.compile("^\\s*((?:19|20)\\d{2})\\-(1[012]|0?[1-9])\\-(3[01]|[12][0-9]|0?[1-9])\\s*$");

    @Override
    public List<Author> getAll() throws DataException {
        List<Author> authors = (List<Author>) authorOperations.findAll();
        if (authors.isEmpty())
            throw new DataException("authors are absent",EMPTY_RESULT);
        logger.info("Authors were gotten");
        return authors;
    }

    @Override
    public Author getAuthor(long authorId) throws DataException {
        Optional<Author> author = authorOperations.findById(authorId);
        if(!author.isPresent()) throw new DataException("Author with such id doesn't exist",AUTHOR_DOES_NOT_EXIST);
        logger.info("Author was get");
        return author.get();
    }

    @Override
    public Author addAuthor(String name, String secondName, String lastName, String dob) throws DataException {
        if(name.isEmpty() || secondName.isEmpty())
            throw new DataException("Empty input",EMPTY_FIELD_INPUT);
        String  date = dob;
        if(!DATE_PATTERN.matcher(dob).matches()) {
            try {
                date =  new SimpleDateFormat("yyyy-mm-dd").format(new SimpleDateFormat("dd.mm.yyyy").parse(dob));
            } catch (ParseException e) {
                throw new DataException("Wrong date format",WRONG_DATE_FORMAT);//TODO tryParse to normal format (if left some time (ofc no))
            }
        }
        logger.info("Author was add");
        return authorOperations.save(new Author(name,secondName,lastName,Date.valueOf(date)));
    }

    @Override
    public void dropAuthorAndAllHisBooks(long authorId) throws DataException {
     if(!authorOperations.findById(authorId).isPresent()) throw new DataException("Author with such id doesn't exist",AUTHOR_DOES_NOT_EXIST);
     authorOperations.deleteAuthorAndAllHisBooks(authorId);
     logger.info("Author  deleted");
    }

}
