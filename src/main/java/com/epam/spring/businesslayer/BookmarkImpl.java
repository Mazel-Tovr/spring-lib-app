package com.epam.spring.businesslayer;


import com.epam.spring.businesslayer.interfaces.IBookmarkService;
import com.epam.spring.dao.interfaces.IBookOperations;
import com.epam.spring.dao.interfaces.IBookmarkOperations;
import com.epam.spring.dao.interfaces.IUserOperation;
import com.epam.spring.exception.DataException;
import com.epam.spring.model.Book;
import com.epam.spring.model.Bookmark;
import com.epam.spring.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.epam.spring.exception.ExceptionsCods.*;

@Service
public class BookmarkImpl implements IBookmarkService {

    @Autowired
    private IBookOperations bookOperations;
    @Autowired
    private IUserOperation userOperation;
    @Autowired
    private IBookmarkOperations bookmarkOperations;

    private static final Logger logger = Logger.getLogger("BusinessLayerLogger");

    @Override
    public Bookmark addBookmark(long userId, long bookId, int pageNumber) throws DataException {

        Optional<User> user = userOperation.findById(userId);
        Optional<Book> book = bookOperations.findById(bookId);
        if (!user.isPresent()) throw new DataException("User with such id doesn't exist", USER_DOES_NOT_EXIST);
        if (!book.isPresent()) throw new DataException("Book with such id doesn't exist", BOOK_DOES_NOT_EXIST);
        if (pageNumber < 0)
            throw new DataException("Page count should be more then 0", PAGE_COUNT_SHOULD_BE_MORE_THEN_ZERO);
        if (book.get().getPageCount() < pageNumber)
            throw new DataException("Book doesn't have such page", BOOK_DOSE_NOT_HAVE_SUCH_PAGE);
        logger.debug("Validation successfully passed");
        logger.info("Bookmark was add");
        return bookmarkOperations.save(new Bookmark(user.get(), book.get(), pageNumber));
    }

    @Override
    public Bookmark getBookmark(long bookmarkId) throws DataException {
        Optional<Bookmark> bookmark = bookmarkOperations.findById(bookmarkId);
        if (!bookmark.isPresent())
            throw new DataException("Bookmark with such id doesn't exist", BOOKMARK_DOES_NOT_EXIST);
        logger.info("Bookmark was get");
        return bookmark.get();
    }

    @Override
    public void dropBookmark(long bookmarkId) throws DataException {
        bookmarkOperations.deleteById(bookmarkId);
        logger.info("Bookmark deleted");
    }

    @Override
    public List<Bookmark> getAllUserBookMark(long userId) throws DataException {
        List<Bookmark> bookmarks = bookmarkOperations.findByUserUserId(userId);
        if (bookmarks.isEmpty()) throw new DataException("Bookmark are absent", EMPTY_RESULT);
        logger.info("Bookmarks were gotten");
        return bookmarks;
    }
}

