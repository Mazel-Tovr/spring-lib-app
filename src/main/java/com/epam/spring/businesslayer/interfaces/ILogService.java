package com.epam.spring.businesslayer.interfaces;

import com.epam.spring.exception.DataException;
import com.epam.spring.model.Log;
import com.epam.spring.model.User;

import java.util.List;

public interface ILogService
{
    List<Log> getAllLogs() throws DataException;
    Log addLog(User user, String message);
}
