package com.epam.spring.businesslayer.interfaces;

import com.epam.spring.exception.UserException;
import com.epam.spring.model.User;

public interface IAdminService extends IUserService {

    User toRegister(String firstName, String lastName, String nickName, String password, int roleId) throws UserException;

    void blockUser(long userId) throws UserException;
    void blockUser(User user) throws UserException;

}
