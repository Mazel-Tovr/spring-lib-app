package com.epam.spring.businesslayer.interfaces;


import com.epam.spring.exception.UserException;
import com.epam.spring.model.User;

public interface IUserService {
    User login(String nickName, String passWord) throws UserException;

    User toRegister(String firstName, String lastName, String nickName, String password) throws UserException;

}
