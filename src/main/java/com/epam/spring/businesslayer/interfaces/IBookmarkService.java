package com.epam.spring.businesslayer.interfaces;


import com.epam.spring.exception.DataException;
import com.epam.spring.model.Bookmark;

import java.util.List;

public interface IBookmarkService
{
    Bookmark addBookmark(long userId, long bookId, int pageNumber) throws DataException;
    Bookmark getBookmark(long bookmarkId) throws DataException;
    void dropBookmark(long bookmarkId) throws DataException;
    List<Bookmark> getAllUserBookMark(long userId) throws DataException;
}
