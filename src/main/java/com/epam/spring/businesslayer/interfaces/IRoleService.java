package com.epam.spring.businesslayer.interfaces;


import com.epam.spring.exception.DataException;
import com.epam.spring.model.EnumRole;
import com.epam.spring.model.Role;

public interface IRoleService
{
    Role getRole(int roleId) throws DataException;
    Role addRole(EnumRole type);
    void dropRole(int roleId) throws DataException;

}
