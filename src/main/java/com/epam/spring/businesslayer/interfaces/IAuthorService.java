package com.epam.spring.businesslayer.interfaces;


import com.epam.spring.exception.DataException;
import com.epam.spring.model.Author;

import java.util.List;

public interface IAuthorService
{
    List<Author> getAll()throws DataException;
    Author getAuthor(long authorId) throws DataException;
    Author addAuthor(String name, String secondName, String lastName, String dob) throws DataException;
    void dropAuthorAndAllHisBooks(long authorId) throws DataException;
}
