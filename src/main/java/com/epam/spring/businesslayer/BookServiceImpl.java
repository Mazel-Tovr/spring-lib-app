package com.epam.spring.businesslayer;


import com.epam.spring.businesslayer.interfaces.IBookService;
import com.epam.spring.dao.interfaces.IAuthorOperations;
import com.epam.spring.dao.interfaces.IBookOperations;
import com.epam.spring.dao.interfaces.IUserOperation;
import com.epam.spring.exception.DataException;
import com.epam.spring.model.Author;
import com.epam.spring.model.Book;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import static com.epam.spring.exception.ExceptionsCods.*;

@Service
public class BookServiceImpl implements IBookService {

    private static final Pattern ISBN_PATTERN = Pattern.compile("\\d{3}\\-\\d{10}");
    private static final Pattern YEAR_PATTERN = Pattern.compile("\\d{1,4}");
    private static final int CURRENT_YEAR = LocalDate.now().getYear();

    @Autowired
    private IBookOperations bookOperations;
    @Autowired
    private IAuthorOperations authorOperations;
    @Autowired
    private IUserOperation userOperation;

    private static final Logger logger = Logger.getLogger("BusinessLayerLogger");

    @Override
    public Book addBook(String bookName, int releaseYear, int pageCount,
                        String ISBN, String publisher, long authorId) throws DataException {

        if (!YEAR_PATTERN.matcher(String.valueOf(releaseYear)).matches() || releaseYear > CURRENT_YEAR)
            throw new DataException("Date input error", WRONG_DATE_FORMAT);
        if (pageCount < 0)
            throw new DataException("Page count should be more then 0", PAGE_COUNT_SHOULD_BE_MORE_THEN_ZERO);
        if (!ISBN_PATTERN.matcher(ISBN).matches())
            throw new DataException("ISBN input error", ISBN_WRONG_FORMAT);//TODO NEED TEST THIS
        Optional<Author> author = authorOperations.findById(authorId);
        if (!author.isPresent())
            throw new DataException("Author with such id doesn't exist", AUTHOR_DOES_NOT_EXIST);
        logger.info("Book added");
        return bookOperations.save(new Book(bookName, releaseYear, pageCount, ISBN, publisher, author.get()));
    }

    @Override
    public Book getBook(long bookId) throws DataException {
        Optional<Book> book = bookOperations.findById(bookId);
        if (!book.isPresent())
            throw new DataException("Book with such id does't exist", BOOK_DOES_NOT_EXIST);
        logger.info("Book gotten");
        return book.get();
    }

    @Override
    public List<Book> getAll() throws DataException {
        List<Book> bookList = (List<Book>) bookOperations.findAll();
        if (bookList.isEmpty())
            throw new DataException("Books are absent", EMPTY_RESULT);
        logger.info("Books gotten");
        return bookList;
    }

    @Override
    public void dropBookById(long bookId) throws DataException {
        if (!bookOperations.findById(bookId).isPresent())
            throw new DataException("Book with such id does't exist", BOOK_DOES_NOT_EXIST);
        bookOperations.deleteById(bookId);
        logger.info("Book deleted");
    }


    @Override
    public List<Book> searchBookByPartOfBookName(String partOfName) throws DataException {
        if (partOfName.isEmpty())
            throw new DataException("Empty input", EMPTY_FIELD_INPUT);
        List<Book> bookList = bookOperations.findAllBookByBookNameContaining(partOfName);
        if (bookList.isEmpty())
            throw new DataException("No matches found", NO_MATCHES_FOUND);
        logger.info("Books gotten");
        return bookList;
    }

    @Override
    public List<Book> searchBookByPartOfAuthorName(String partOfAuthorName) throws DataException {
        if (partOfAuthorName.isEmpty())
            throw new DataException("Empty input", EMPTY_FIELD_INPUT);
        List<Book> bookList = bookOperations.findAllBookByAuthorNameContaining(partOfAuthorName);
        if (bookList.isEmpty())
            throw new DataException("No matches found", NO_MATCHES_FOUND);
        logger.info("Books gotten");
        return bookList;
    }

    @Override
    public Book searchBookByISBN(String ISBN) throws DataException {
        if (ISBN.isEmpty())
            throw new DataException("Empty input", EMPTY_FIELD_INPUT);
        Book book = bookOperations.findByISBN(ISBN);
        if (book == null)
            throw new DataException("No matches found", NO_MATCHES_FOUND);
        logger.info("Books gotten");
        return book;
    }

    @Override
    public List<Book> searchBookByYearRange(int from, int to) throws DataException {
        if ((!YEAR_PATTERN.matcher(String.valueOf(from)).matches() || from > CURRENT_YEAR) ||
                (!YEAR_PATTERN.matcher(String.valueOf(to)).matches() || to > CURRENT_YEAR))
            throw new DataException("Date input error", WRONG_DATE_FORMAT);
        List<Book> bookList = bookOperations.findByReleaseYearBetween(from, to);
        if (bookList.isEmpty())
            throw new DataException("No matches found", NO_MATCHES_FOUND);
        logger.info("Books gotten");
        return bookList;
    }

    @Override
    public List<Book> searchBookByYearAndPageCountAndPartName(int year, int pageCount, String partOfName) throws DataException {
        if (!YEAR_PATTERN.matcher(String.valueOf(year)).matches() || year > CURRENT_YEAR)
            throw new DataException("Date input error", WRONG_DATE_FORMAT);
        if (pageCount < 0)
            throw new DataException("Page count should be more then 0", PAGE_COUNT_SHOULD_BE_MORE_THEN_ZERO);
        List<Book> bookList = bookOperations.findAllByReleaseYearAndPageCountAndBookNameContaining(year, pageCount, partOfName);
        if (bookList.isEmpty())
            throw new DataException("No matches found", NO_MATCHES_FOUND);
        logger.info("Books gotten");
        return bookList;
    }

    @Override
    public List<Book> searchBookWhereUserBookMark(long userId) throws DataException {
        if (!userOperation.findById(userId).isPresent())
            throw new DataException("User with such id doesn't exist", USER_DOES_NOT_EXIST);
        List<Book> bookList = bookOperations.findAllBookByUserBookMark(userId);
        if (bookList.isEmpty())
            throw new DataException("No matches found", NO_MATCHES_FOUND);
        logger.info("Books gotten");
        return bookList;
    }

}
