package com.epam.spring.businesslayer;


import com.epam.spring.businesslayer.interfaces.IAdminService;
import com.epam.spring.businesslayer.interfaces.ILogService;
import com.epam.spring.dao.interfaces.IRoleOperations;
import com.epam.spring.dao.interfaces.IUserOperation;
import com.epam.spring.exception.UserException;
import com.epam.spring.model.Role;
import com.epam.spring.model.User;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.epam.spring.exception.ExceptionsCods.*;

@Service
public class UserServiceImpl implements IAdminService {

    @Autowired
    private IUserOperation userOperation;
    @Autowired
    private IRoleOperations roleOperations;

    private static final Logger logger = Logger.getLogger("BusinessLayerLogger");
    @Autowired
    private ILogService logService ;

    //Registration by admin
    @Override
    public User toRegister(String firstName, String lastName, String nickName, String password, int roleId) throws UserException {
        if(nickName.isEmpty() || password.isEmpty()) throw new UserException("Nick name and password can't be empty", EMPTY_FIELD_INPUT);
        String hexPassword = DigestUtils.md2Hex(password);
        if (userOperation.existsByNickName(nickName)) throw new UserException("User with such Nickname already exist",USER_DOES_NOT_EXIST);
        Optional<Role> role = roleOperations.findById(roleId);
        if (!role.isPresent()) throw new UserException("Such role doesn't exist", ROLE_DOES_NOT_EXIST);
        logger.debug("Register validation successfully passed");
        logger.info("User successfully register");
        return userOperation.save(new User(firstName, lastName, nickName, hexPassword, role.get(),false));

    }

    @Override
    public void blockUser(long userId) throws UserException {
        Optional<User> user = userOperation.findById(userId);
        if(!user.isPresent()) throw new UserException("User with such id does't exist",USER_DOES_NOT_EXIST);
        blockUser(user.get());
    }



    @Override
    public void blockUser(User user) throws UserException {
        if (user.isBlocked()) throw new UserException("Current user already blocked",USER_ALREADY_BLOCKED);
        logger.debug("BlockUser validation successfully passed");
        userOperation.blockUser(user.getUserId());
        logService.addLog(user, "User: "+user.getNickName()+ " was blocked");
        logger.info("User successfully blocked");
    }

    @Override
    public User login(String nickName, String password) throws UserException {
        String hexPassword = DigestUtils.md2Hex(password);
        if (!userOperation.existsByNickName(nickName)) throw new UserException("User with such Nickname doesn't exist",USER_DOES_NOT_EXIST);
        User user = userOperation.findByNickNameAndPassword(nickName, hexPassword);
        if (user == null) throw new UserException("Wrong password, try again ",WRONG_PASSWORD);
        if (user.isBlocked()) throw new UserException("you are blocked",USER_ALREADY_BLOCKED);
        logger.debug("Login validation successfully passed");
        logger.info("Your are successfully logging ");
        logService.addLog(user, "User: "+user.getNickName()+ " entered to lib");
        return user;
    }

    //Registration by user
    @Override
    public User toRegister(String firstName, String lastName, String nickName, String password) throws UserException {
        if(nickName.isEmpty() || password.isEmpty()) throw new UserException("Nick name and password can't be empty", EMPTY_FIELD_INPUT);
        String hexPassword = DigestUtils.md2Hex(password);
        if (userOperation.existsByNickName(nickName)) throw new UserException("User with such nickName already exist",USER_ALREADY_EXIST);
        logger.debug("Register validation successfully passed");
        logger.info("You are successfully register");
        return userOperation.save(new User(firstName, lastName, nickName, hexPassword, roleOperations.findById(1).get(),false));

    }
}
