package com.epam.spring.businesslayer;


import com.epam.spring.businesslayer.interfaces.ILogService;
import com.epam.spring.dao.interfaces.ILogOperations;
import com.epam.spring.exception.DataException;
import com.epam.spring.model.Log;
import com.epam.spring.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.epam.spring.exception.ExceptionsCods.*;

@Service
public class LogServiceImpl implements ILogService
{
    @Autowired
    private ILogOperations logOperations;

    private static final Logger logger = Logger.getLogger("BusinessLayerLogger");

    @Override
    public List<Log> getAllLogs() throws DataException {
        List<Log> logList = (List<Log>) logOperations.findAll();
        if (logList.isEmpty()) throw new DataException("Logs are absent",EMPTY_RESULT);
        logger.info("Logs gotten");
        return logList;
    }

    @Override
    public Log addLog(User user, String message) {
        logger.info("Log added");
        return logOperations.save(new Log(user,message));
    }
}
