package com.epam.spring;

import com.epam.spring.businesslayer.interfaces.*;
import com.epam.spring.dao.interfaces.IAuthorOperations;
import com.epam.spring.dao.interfaces.IBookOperations;
import com.epam.spring.dao.interfaces.IUserOperation;
import com.epam.spring.exception.DataException;
import com.epam.spring.exception.UserException;
import com.epam.spring.model.Author;
import com.epam.spring.model.Book;
import com.epam.spring.model.Bookmark;
import com.epam.spring.model.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.text.SimpleDateFormat;

@SpringBootApplication
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }


    @Bean
    public CommandLineRunner demo(IAuthorService authorService,
                                  IBookService bookService,
                                  IAdminService userOperation,
                                  IBookmarkService bookmarkService) {
        return (args) -> {
            //  authorServiceTest(authorService);
            // bookmarkServiceTest(bookmarkService);
            // bookServiceTest(bookService);
            // userServiceTest(userOperation);
        };

    }

    private static void authorServiceTest(IAuthorService authorService) {
        try {
            Author author = authorService.addAuthor("AU-THOR", "Gfg", "gf", "2001-11-23");
            authorService.getAll().forEach(System.out::println);
            System.out.println("----------------------------------------------------------------");
            authorService.dropAuthorAndAllHisBooks(author.getAuthorId());
            authorService.getAll().forEach(System.out::println);
            System.out.println("----------------------------------------------------------------");
            System.out.println(authorService.getAuthor(2L).toString());

        } catch (DataException e) {
            e.printStackTrace();
        }
    }

    private static void bookmarkServiceTest(IBookmarkService bookmarkService) {
        try {
            bookmarkService.getAllUserBookMark(2).forEach(System.out::println);
            System.out.println("----------------------------------------------------------------");
            Bookmark bookmark = bookmarkService.addBookmark(2, 15, 12);
            System.out.println("----------------------------------------------------------------");
            bookmarkService.getAllUserBookMark(2).forEach(System.out::println);
            System.out.println("----------------------------------------------------------------");
            System.out.println(bookmarkService.getBookmark(bookmark.getBookmarkId()));
            System.out.println("----------------------------------------------------------------");
            bookmarkService.dropBookmark(bookmark.getBookmarkId());
            bookmarkService.getAllUserBookMark(2).forEach(System.out::println);
            System.out.println("----------------------------------------------------------------");

        } catch (DataException e) {
            e.printStackTrace();
        }
    }

    private static void bookServiceTest(IBookService bookService) {
        try {
            bookService.getAll().forEach(System.out::println);
            System.out.println("----------------------------------------------------------------");
            Book book = bookService.addBook("book1", 123, 123, "000-0000000003", "books", 4);
            bookService.getAll().forEach(System.out::println);
            System.out.println("----------------------------------------------------------------");
            System.out.println(bookService.getBook(book.getBookId()));
            System.out.println("----------------------------------------------------------------");
            bookService.dropBookById(book.getBookId());
            bookService.getAll().forEach(System.out::println);
            System.out.println("----------------------------------------------------------------");
            System.out.println(bookService.searchBookByISBN("978-0452261342").toString());
            System.out.println("----------------------------------------------------------------");
            System.out.println(bookService.searchBookByPartOfAuthorName("ut"));
            System.out.println("----------------------------------------------------------------");
            System.out.println(bookService.searchBookByPartOfBookName("q"));
            System.out.println("----------------------------------------------------------------");
            System.out.println(bookService.searchBookByYearRange(1, 2020));
            System.out.println("----------------------------------------------------------------");
            System.out.println(bookService.searchBookByYearAndPageCountAndPartName(2012, 333, "qwe"));
            System.out.println("----------------------------------------------------------------");
            System.out.println(bookService.searchBookWhereUserBookMark(6));
            System.out.println("----------------------------------------------------------------");
        } catch (DataException e) {
            e.printStackTrace();
        }
    }

    private static void userServiceTest(IAdminService userService) {
        try {
            User user = userService.toRegister("Roma", "AAO", "one", "two", 1);
            System.out.println(userService.login("one", "two").toString());
            userService.blockUser(user);
        } catch (UserException e) {
            e.printStackTrace();
        }
    }
}
