package com.epam.spring.audit;

import com.epam.spring.dao.interfaces.IAuditLogOperations;
import com.epam.spring.exception.UserException;
import com.epam.spring.model.AuditLog;
import com.epam.spring.model.User;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

@Aspect
@Component
public class UserServiceAudit
{

//  @Autowired
//  private CurrentUser currentUser; //to extend audit

  @Autowired
  private IAuditLogOperations auditLogOperations;

  @Pointcut("execution (* com.epam.spring.businesslayer.UserServiceImpl.toRegister(..))")
  public void registerPointcut() { }
  @Pointcut("execution (* com.epam.spring.businesslayer.UserServiceImpl.login(..))")
  public void loginPointCut(){}
  @Pointcut("execution (* com.epam.spring.businesslayer.UserServiceImpl.blockUser(..))")
  public void blockPointCut(){}

  @Before("registerPointcut() || loginPointCut() || blockPointCut()")
  public void before(JoinPoint joinPoint)
  {
      StringBuilder log = new StringBuilder();
      log.append("Method called: ")
         .append(joinPoint.getSignature().getName())
         .append(" with args: ")
         .append(Arrays.stream(joinPoint.getArgs())
                .map(Object::toString)
                .collect(Collectors.joining(",")));
    auditLogOperations.save(new AuditLog(log.toString()));
  }
  @AfterThrowing(pointcut = "registerPointcut() || loginPointCut() || blockPointCut()",throwing = "ex")
  public void afterException(JoinPoint joinPoint, UserException ex)
  {
    StringBuilder log = new StringBuilder();
    log.append("Method called: ")
            .append(joinPoint.getSignature().getName())
            .append(" Threw exception: ")
            .append(ex.toString());
    auditLogOperations.save(new AuditLog(log.toString()));
  }
  @AfterReturning(pointcut ="registerPointcut()",returning = "user")
  public void afterNoneExceptionRegister(JoinPoint joinPoint,User user)
  {
    StringBuilder log = new StringBuilder();
    log.append("Method: ")
            .append(joinPoint.getSignature().getName())
            .append(" successfully fulfilled ")
            .append(" User: ").append(user.getNickName())
            .append(" register");
    auditLogOperations.save(new AuditLog(log.toString()));
  }

  @AfterReturning(pointcut = "loginPointCut()",returning = "user")
  public void afterNoneExceptionLogin(JoinPoint joinPoint, User user)
  {
    StringBuilder log = new StringBuilder();
    log.append("Method: ")
            .append(joinPoint.getSignature().getName())
            .append(" successfully fulfilled ")
            .append("User: Id ")
            .append(user.getUserId()).append(" Nick name").append(user.getNickName())
            .append(" logged in");
    auditLogOperations.save(new AuditLog(log.toString()));
  }

  @AfterReturning("blockPointCut()")
  public void afterNoneExceptionBlockUser(JoinPoint joinPoint)
  {
    StringBuilder log = new StringBuilder();
    log.append("Method: ")
            .append(joinPoint.getSignature().getName())
            .append(" successfully fulfilled ")
            .append("User: ")
            .append((Arrays.stream(joinPoint.getArgs())
                    .map(Object::toString)
                    .collect(Collectors.joining(","))))
            .append(" blocked");
    auditLogOperations.save(new AuditLog(log.toString()));
  }

}
