package com.epam.spring.audit;

import com.epam.spring.dao.interfaces.IAuditLogOperations;
import com.epam.spring.exception.DataException;
import com.epam.spring.model.AuditLog;
import com.epam.spring.model.Book;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

@Aspect
@Component
public class BookServiceAudit {

    @Autowired
    private IAuditLogOperations auditLogOperations;

    @Pointcut("execution (* com.epam.spring.businesslayer.BookServiceImpl.dropBookById(..))")
    public void dropBookByIdPointCut(){}
    @Pointcut("execution (* com.epam.spring.businesslayer.BookServiceImpl.addBook(..))")
    public void addBook(){}

    @Before("dropBookByIdPointCut() || addBook()")
    public void before(JoinPoint joinPoint)
    {
        StringBuilder log = new StringBuilder();
        log.append("Method called: ")
                .append(joinPoint.getSignature().getName())
                .append(" with args: ")
                .append(Arrays.stream(joinPoint.getArgs())
                        .map(Object::toString)
                        .collect(Collectors.joining(",")));
        auditLogOperations.save(new AuditLog(log.toString()));
    }
    @AfterThrowing(pointcut = "dropBookByIdPointCut() || addBook()",throwing = "ex")
    public void afterException(JoinPoint joinPoint, DataException ex)
    {
        StringBuilder log = new StringBuilder();
        log.append("Method called: ")
                .append(joinPoint.getSignature().getName())
                .append(" Threw exception: ")
                .append(ex.toString());
        auditLogOperations.save(new AuditLog(log.toString()));
    }

    @AfterReturning("dropBookByIdPointCut()")
    public void afterNoneExceptionDropBook(JoinPoint joinPoint)
    {
        StringBuilder log = new StringBuilder();
        log.append("Method: ")
                .append(joinPoint.getSignature().getName())
                .append(" successfully fulfilled ")
                .append("book with id:").append(joinPoint.getArgs()[0])
                .append(" successfully deleted");
        auditLogOperations.save(new AuditLog(log.toString()));
    }

    @AfterReturning(pointcut ="addBook()",returning = "book")
    public void afterNoneExceptionAddBook(JoinPoint joinPoint, Book book)
    {
        StringBuilder log = new StringBuilder();
        log.append("Method: ")
                .append(joinPoint.getSignature().getName())
                .append(" successfully fulfilled ")
                .append(book.toString())
                .append(" successfully add");
        auditLogOperations.save(new AuditLog(log.toString()));
    }
}
