package com.epam.spring.audit;

import com.epam.spring.dao.interfaces.IAuditLogOperations;
import com.epam.spring.exception.DataException;
import com.epam.spring.model.AuditLog;
import com.epam.spring.model.Bookmark;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

@Aspect
@Component
public class BookmarkServiceAudit
{
    @Autowired
    private IAuditLogOperations auditLogOperations;

    @Pointcut("execution (* com.epam.spring.businesslayer.BookmarkImpl.addBookmark(..)) ")
    public void addBookMarkPointCut(){}
    @Pointcut("execution (* com.epam.spring.businesslayer.BookmarkImpl.dropBookmark(..))")
    public void dropBookmarkPointCut(){}


    @Before("addBookMarkPointCut() || dropBookmarkPointCut()")
    public void before(JoinPoint joinPoint)
    {
        StringBuilder log = new StringBuilder();
        log.append("Method called: ")
                .append(joinPoint.getSignature().getName())
                .append(" with args: ")
                .append(Arrays.stream(joinPoint.getArgs())
                        .map(Object::toString)
                        .collect(Collectors.joining(",")));
        auditLogOperations.save(new AuditLog(log.toString()));
    }
    @AfterThrowing(pointcut = "addBookMarkPointCut() || dropBookmarkPointCut()",throwing = "ex")
    public void afterException(JoinPoint joinPoint, DataException ex)
    {
        StringBuilder log = new StringBuilder();
        log.append("Method called: ")
                .append(joinPoint.getSignature().getName())
                .append(" Threw exception: ")
                .append(ex.toString());
        auditLogOperations.save(new AuditLog(log.toString()));
    }

    @AfterReturning(pointcut ="addBookMarkPointCut()" ,returning = "bookmark")
    public void afterNoneExceptionAddBookMark(JoinPoint joinPoint, Bookmark bookmark)
    {
        StringBuilder log = new StringBuilder();
        log.append("Method: ")
                .append(joinPoint.getSignature().getName())
                .append(" successfully fulfilled ")
                .append(bookmark.toString())
                .append(" bookmark added");
        auditLogOperations.save(new AuditLog(log.toString()));
    }

    @AfterReturning("dropBookmarkPointCut()")
    public void afterNoneException(JoinPoint joinPoint)
    {
        StringBuilder log = new StringBuilder();
        log.append("Method: ")
                .append(joinPoint.getSignature().getName())
                .append(" successfully fulfilled ")
                .append("Bookmark with id: ")
                .append(joinPoint.getArgs()[0])
                .append(" bookmark added");
        auditLogOperations.save(new AuditLog(log.toString()));
    }
}
