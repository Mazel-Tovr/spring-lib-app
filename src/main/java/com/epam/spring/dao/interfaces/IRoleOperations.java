package com.epam.spring.dao.interfaces;

import com.epam.spring.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface IRoleOperations extends CrudRepository<Role,Integer> {
}
