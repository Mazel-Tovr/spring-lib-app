package com.epam.spring.dao.interfaces;


import com.epam.spring.model.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface IUserOperation extends CrudRepository<User,Long> {

    boolean existsByNickName(String nickName);

//    @Modifying
//    @Query("SELECT u FROM User u WHERE u.nickName = ?1 AND u.password = ?2")
//    User getUser(String nickName, String password);
    User findByNickNameAndPassword(String nickName, String password);

    //0-nothing happened 1-user blocked
    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.isBlocked=true WHERE u.userId = ?1")
    int blockUser(long userId);
}