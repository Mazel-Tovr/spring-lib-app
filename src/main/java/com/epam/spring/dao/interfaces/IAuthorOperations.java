package com.epam.spring.dao.interfaces;


import com.epam.spring.model.Author;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IAuthorOperations extends CrudRepository<Author,Long>
{
    @Modifying
    @Query("DELETE FROM Author a where a.authorId = ?1")
    @Transactional
    int deleteAuthorAndAllHisBooks(long authorId);
}
