package com.epam.spring.dao.interfaces;


import com.epam.spring.model.Bookmark;
import com.epam.spring.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IBookmarkOperations extends CrudRepository<Bookmark,Long>
{
    List<Bookmark> findByUser(User user);
    List<Bookmark> findByUserUserId(long userId);

}
