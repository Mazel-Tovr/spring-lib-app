package com.epam.spring.dao.interfaces;

import com.epam.spring.model.Log;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ILogOperations extends CrudRepository<Log,Long>
{

}
