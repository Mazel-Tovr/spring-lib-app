package com.epam.spring.dao.interfaces;

import com.epam.spring.model.AuditLog;
import org.springframework.data.repository.CrudRepository;

public interface IAuditLogOperations extends CrudRepository<AuditLog,Long> {
}
