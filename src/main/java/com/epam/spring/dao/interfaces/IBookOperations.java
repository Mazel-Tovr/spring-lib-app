package com.epam.spring.dao.interfaces;


import com.epam.spring.model.Book;
import com.epam.spring.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IBookOperations extends CrudRepository<Book,Long>
{

    List<Book> findAllBookByBookNameContaining(String bookName);

    List<Book> findAllBookByAuthorNameContaining(String AuthorName);
    Book findByISBN(String ISBN);
    List<Book> findByReleaseYearBetween(int from, int to);
    List<Book> findAllByReleaseYearAndPageCountAndBookNameContaining(int year, int pageCount, String bookName);

    List<Book> findAllBookByUserBookMark(long userId);

}
